module Day3 () where

import Data.List (transpose)

type Row = [Int]
data Triangle = Triangle Int Int Int deriving Show

triangles :: [Row] -> [Triangle]
triangles [] = []
triangles ([]:rest) = triangles rest
triangles ((a:b:c:row):rest) = Triangle a b c : triangles (row:rest)

validTriangle :: Triangle -> Bool
validTriangle (Triangle a b c) = and [((a + b) > c), ((a + c) > b), ((b + c) > a)]

main :: IO ()
main = do
  rawInput <- readFile "day_3.txt"
  let rows = map ((map read) . words) $ lines rawInput
  let rowTriangles = triangles rows
  print $ "Row triangles: " ++ (show . length $ filter validTriangle rowTriangles)
  let columnTriangles = triangles $ transpose rows
  print $ "Column triangles: " ++ (show . length $ filter validTriangle columnTriangles)
