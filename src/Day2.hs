module Day2 () where
type ButtonGrid = [[Char]]
type KeyCode = String

buttonGrid1 :: ButtonGrid
buttonGrid1 = [
    "123",
    "456",
    "789"
  ]

buttonGrid2 :: ButtonGrid
buttonGrid2 =
  [
    "__1__",
    "_234_",
    "56789",
    "_ABC_",
    "__D__"
  ]

middleOf :: Int -> Int
middleOf = round . (/2) . fromIntegral

centerOf :: ButtonGrid -> Coordinates
centerOf grid =
  let
    width = length $ grid !! 0
    height = length grid
  in Coordinates (middleOf width) (middleOf height)

data Coordinates = Coordinates {x :: Int, y :: Int} deriving (Show)
data Step = U | D | L | R deriving (Show)

parseStep :: Char -> Step
parseStep step = case step of
  'U' -> U
  'D' -> D
  'L' -> L
  'R' -> R

outOfBounds :: ButtonGrid -> Coordinates -> Bool
outOfBounds grid position@(Coordinates x y) =
  if (x < 0) || (y < 0) || (y >= length grid) || (x >= (length $ grid !! 0))
  then True
  else case keyAt grid position of
    '_' -> True
    _ -> False

applyMove :: ButtonGrid -> Coordinates -> Step -> Coordinates
applyMove grid start step =
  let
    (Coordinates x y) = start
    newX = case step of
      L -> x-1
      R -> x+1
      _ -> x
    newY = case step of
      U -> y-1
      D -> y+1
      _ -> y
    finish = Coordinates newX newY
  in if outOfBounds grid finish then start else finish

keyAt :: ButtonGrid -> Coordinates -> Char
keyAt grid (Coordinates x y) = (grid !! y) !! x

findKey :: ButtonGrid -> Coordinates -> [Step] -> (Char, Coordinates)
findKey grid start [] = (keyAt grid start, start)
findKey grid start (step:steps) =
  let nextStart = applyMove grid start step
  in findKey grid nextStart steps

findAllKeys :: ButtonGrid -> Coordinates -> [[Step]] -> KeyCode
findAllKeys grid start [] = ""
findAllKeys grid start (path:paths) =
  let (key, keyLocation) = findKey grid start path
  in key : (findAllKeys grid keyLocation paths)

stepsToCode :: ButtonGrid -> [[Step]] -> KeyCode
stepsToCode grid paths = findAllKeys grid (centerOf grid) paths

main :: IO ()
main = do
  rawInput <- readFile "day_2.txt"
  let buttonPaths = map (map parseStep) $ lines rawInput
  print $ "Door code 1: " ++ (stepsToCode buttonGrid1 buttonPaths)
  print $ "Door code 2: " ++ (stepsToCode buttonGrid2 buttonPaths)
