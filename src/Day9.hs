module Day9 () where

import Text.Regex (mkRegex, matchRegexAll)

main :: IO ()
main = do
  raw <- readFile "day_9.txt"
  let decompressed = decompress $ init raw
  print $ "Decompressed length: " ++ (show . length $ decompressed)

decompress :: String -> String
decompress [] = []
decompress (c:cs) = case c of
  '(' ->
    let (expanded, rest) = expandRepeat cs
    in expanded ++ decompress rest
  _ -> c : decompress cs

expandRepeat :: String -> (String, String)
expandRepeat str =
  let (amount, times, rest) = eatExpand str
  in (multiply (take amount rest) times, (drop amount rest))

eatExpand :: String -> (Int, Int, String)
eatExpand str =
  case matchRegexAll (mkRegex "^([0-9]+)x([0-9]+)\\)") str of
  Just match ->
    let (_, _, rest, [amount, times]) = match
    in (read amount, read times, rest)
  Nothing -> error $ "Invalid token: " ++ (take 20 str)

multiply :: [a] -> Int -> [a]
multiply l 1 = l
multiply l n = l ++ multiply l (n-1)
