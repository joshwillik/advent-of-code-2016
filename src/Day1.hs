module Day1 () where
normalizeStep :: String -> String
normalizeStep step = if last step == ',' then init step else step

data DirectionChange = L | R deriving (Show)
data Step = Step DirectionChange Int deriving (Show)

parseStep :: String -> Step
parseStep (direction:steps) = Step (parseDirectionChange direction) (read steps)

parseDirectionChange :: Char -> DirectionChange
parseDirectionChange x = if x == 'R' then R else L

data Direction = N | E | W | S deriving (Show, Enum)
data Coordinates = Coordinates { x::Int, y::Int } deriving (Show, Eq)
data Position = Position Coordinates Direction Int deriving Show

coords :: Position -> Coordinates
coords (Position x _ _) = x

addStep :: [Position] -> Step -> [Position]
addStep (lastPosition:positions) (Step change stepDistance) =
  Position (Coordinates newX newY) newDir stepDistance : lastPosition : positions
  where
    (Position (Coordinates x y) dir _) = lastPosition
    newDir = rotate dir change
    newX = case newDir of
      W -> x-stepDistance
      E -> x+stepDistance
      _ -> x
    newY = case newDir of
      N -> y+stepDistance
      S -> y-stepDistance
      _ -> y

rotate :: Direction -> DirectionChange -> Direction
-- is there a fromEnum/toEnum way to handle this?
rotate N R = E
rotate E R = S
rotate W R = N
rotate S R = W
rotate N L = W
rotate E L = N
rotate W L = S
rotate S L = E

firstDuplicate :: [Coordinates] -> [Coordinates] -> Coordinates
firstDuplicate _ [] = error "No duplicate found"
firstDuplicate soFar (current:next) = if current `elem` soFar
  then current
  else firstDuplicate (current:soFar) next

allCoordinates :: [Coordinates] -> [Coordinates]
allCoordinates (a:b:[]) = (allCoordinatesUpTo a b)
allCoordinates (a:b:others) = (allCoordinatesUpTo a b) ++ allCoordinates (b:others)

allCoordinatesUpTo :: Coordinates -> Coordinates -> [Coordinates]
allCoordinatesUpTo start end
  | start == end = []
  | isDiagonal start end = error "Cannot find coordinates on diagonal"
  | (x start) == (x end) =
    let nextY = moveTowards (y start) (y end)
    in start : allCoordinatesUpTo (start { y = nextY }) end
  | (y start) == (y end) =
    let nextX = moveTowards (x start) (x end)
    in start : allCoordinatesUpTo (start { x = nextX }) end

isDiagonal :: Coordinates -> Coordinates -> Bool
isDiagonal (Coordinates aX aY) (Coordinates bX bY) = aX /= bX && aY /= bY

moveTowards :: Int -> Int -> Int
moveTowards from to = if from > to then from - 1 else from + 1

main :: IO ()
main = do
  rawSteps <- readFile "day_1.txt"
  let steps = map (parseStep . normalizeStep) $ words rawSteps
  let positions = reverse $ foldl addStep [(Position (Coordinates 0 0) N 0)] steps
  let allPositions = allCoordinates $ map coords positions
  let endDistance = x'+y' where (Position (Coordinates x' y') _ _) = last positions
  print $ "End distance: " ++ (show endDistance)
  let duplicate = firstDuplicate [] allPositions
  print $ "First duplicate: " ++ (show duplicate) ++ " " ++ (show $ (x duplicate) + (y duplicate))
