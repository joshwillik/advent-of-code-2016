module Day5 () where
import Data.Hash.MD5 (Str(Str), md5s)
import Control.Monad (guard)
import Data.List (isPrefixOf)
import Data.Maybe (Maybe(Just, Nothing), catMaybes)
seed = "reyedfim"

hashes :: [String]
hashes = do
  index <- [0..]
  let hash = md5s $ Str (seed ++ (show index))
  guard ("00000" `isPrefixOf` hash)
  guard ((hash !! 5) `elem` ['0'..'7'])
  return hash

getPassword :: [String] -> Int -> [Char]
getPassword hashes len =
  let charArray = fillPassword (emptyPassword len) hashes
  in catMaybes charArray

emptyPassword :: Int -> [Maybe a]
emptyPassword len = (take len $ repeat Nothing)

fillPassword :: [Maybe Char] -> [String] -> [Maybe Char]
fillPassword password hashes
  | passwordFinished password = password
  | otherwise =
    let
      next = head hashes
      position = read $ [next !! 5]
      char = next !! 6
      newPassword = case (password !! position) of
        Just x -> password
        Nothing -> set password position $ Just char
    in fillPassword newPassword $ tail hashes

set :: [a] -> Int -> a -> [a]
set arr index value = (take index arr) ++ [value] ++ (drop (index+1) arr)

passwordFinished :: [Maybe Char] -> Bool
passwordFinished password = (length $ catMaybes password) == length password

main :: IO ()
main = do
  print $ getPassword hashes 8
