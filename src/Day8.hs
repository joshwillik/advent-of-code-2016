module Day8 () where

import Data.List (transpose, isPrefixOf)
import Text.Regex (mkRegex, matchRegex, Regex)

main :: IO ()
main = do
  raw <- readFile "day_8.txt"
  let instructions = map parseInstruction $ lines raw
  let lastScreen = foldl applyInstruction (screen 50 6) instructions
  putStrLn $ formatScreen lastScreen
  putStrLn $ "ON pixels: " ++ ((show . length) $ filter (==On) (concat lastScreen))

data Pixel = On | Off deriving (Show, Eq)
type Screen = [[Pixel]]

screen :: Int -> Int -> Screen
screen x y = screenOf x y Off

screenOf :: Int -> Int -> Pixel -> Screen
screenOf x y v = take y $ repeat $ take x $ repeat v

formatScreen :: Screen -> String
formatScreen screen = unlines $ (map . map) formatPixel screen

formatPixel :: Pixel -> Char
formatPixel On = '#'
formatPixel Off = '.'

data Instruction = Rect {x::Int, y::Int}
                  | ShiftRow {num::Int, amount::Int}
                  | ShiftColumn {num::Int, amount::Int}
                  deriving Show

parseInstruction :: String -> Instruction
parseInstruction str
  | "rect" `isPrefixOf` str = parseToken Rect "rect ([0-9]+)x([0-9]+)" str
  | "rotate row" `isPrefixOf` str = parseToken ShiftRow "rotate row y=([0-9]+) by ([0-9]+)" str
  | "rotate column" `isPrefixOf` str = parseToken ShiftColumn "rotate column x=([0-9]+) by ([0-9]+)" str
  | otherwise = error $ "Invalid instruction: " ++ str

parseToken :: (Int -> Int -> Instruction) -> String -> String -> Instruction
parseToken instruction regex str = case matchRegex (re regex) str of
  Just [a, b] -> instruction (read a) (read b)
  Nothing -> error $ "No match for: " ++ regex ++ " in " ++ str

applyInstruction :: Screen -> Instruction -> Screen
applyInstruction screen (Rect x y) =
  let mergeLine = merge $ take x $ repeat On
  in (map mergeLine $ take y screen) ++ drop y screen
applyInstruction screen (ShiftRow n amount) =
  (take n screen) ++ [shift (screen !! n) amount] ++ (drop (n+1) screen)
applyInstruction screen (ShiftColumn n amount) =
  transpose $ applyInstruction (transpose screen) (ShiftRow n amount)

shift :: [a] -> Int -> [a]
shift row n =
  let l = length row
  in take l $ drop (l-n) $ cycle row

merge :: [a] -> [a] -> [a]
merge [] b = b
merge (a:as) (b:bs) = a : merge as bs

slice :: [a] -> Int -> Int -> [a]
slice xs from to = take (to-from) $ drop from xs

re :: String -> Regex
re = mkRegex
