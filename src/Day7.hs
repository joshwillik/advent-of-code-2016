module Day7 () where

import Text.Regex (mkRegex, matchRegex)
import Data.List (elemIndex, partition, isInfixOf)

main :: IO ()
main = do
  raw <- readFile "day_7.txt"
  let ips = map parseIP $ lines raw
  print $ "Supports TLS: " ++ (show $ length $ filter supportsTls ips)
  print $ "Supports SSL: " ++ (show $ length $ filter supportsSsl ips)

data Net = Supernet String | Hypernet String deriving Show

unwrapNet :: Net -> String
unwrapNet (Supernet x) = x
unwrapNet (Hypernet x) = x

unwrapNets :: [Net] -> [String]
unwrapNets = map unwrapNet

data IP7 = IP7 {rawIp :: String, nets :: [Net], hypernets :: [Net]} deriving Show

supportsTls :: IP7 -> Bool
supportsTls (IP7 _ nets hypernets) =
  let hasAbba x = (>0) . length $ searchForAll isAbba 4 x
  in any hasAbba (unwrapNets nets) && (not $ any hasAbba (unwrapNets hypernets))

supportsSsl :: IP7 -> Bool
supportsSsl (IP7 _ nets hypernets) =
  let
    getAbas :: String -> [String]
    getAbas = searchForAll isAba 3
    abas :: [String]
    abas = concat $ map getAbas (unwrapNets nets)
    babs :: [String]
    babs = map invertAba abas
    hasBab :: String -> Bool
    hasBab x = any (\bab -> isInfixOf bab x) babs
    hns = unwrapNets hypernets
  in any hasBab hns

searchForAll :: (String -> Bool) -> Int -> String -> [String]
searchForAll f len str
  | length str < len = []
  | f $ take len str = take len str : searchForAll f len (drop 1 str)
  | otherwise = searchForAll f len (drop 1 str)

isAbba :: String -> Bool
isAbba [a, b, c, d] = a /= b && [a, b] == [d, c]

isAba :: String -> Bool
isAba [a, b, c] = a /= b && a == c

invertAba :: String -> String
invertAba [a, b, _] = [b, a, b]

parseIP :: String -> IP7
parseIP str =
  let
    parts = parseParts [] str
    (nets, hypernets) = partition isSupernet parts
  in IP7 str nets hypernets

isSupernet :: Net -> Bool
isSupernet (Supernet _) = True
isSupernet _ = False

parseParts :: [Net] -> String -> [Net]
parseParts acc "" = acc
parseParts acc string =
  let (nextPart, remainder) = eatPart string
  in (nextPart : acc) ++ parseParts acc remainder

eatPart :: String -> (Net, String)
eatPart str =
  let
    first = str !! 0
    (token, rest) = case first of
      '[' -> eatTo (drop 1 str) ']'
      _ -> eatTo str '['
    part = case first of
      '[' -> Hypernet token
      _ -> Supernet token
  in (part, rest)

eatTo :: Eq a => [a] -> a -> ([a], [a])
eatTo arr item =
  let n = case elemIndex item arr of
          Nothing -> length arr
          Just index -> index
  in (take n arr, drop n arr)
