module Day6 () where

import Data.List (transpose, sortOn)

main :: IO ()
main = do
  raw <- readFile "day_6.txt"
  let charLines = transpose $ lines raw
  print $ "Most commond password: " ++ (map mostCommon charLines)
  print $ "Least commond password: " ++ (map leastCommon charLines)

mostCommon :: String -> Char
mostCommon str = fst . last $ countChars str

leastCommon :: String -> Char
leastCommon str = fst . head $ countChars str

count :: String -> Char -> Int
count str char = length $ filter (char ==) str

countChars :: String -> [(Char, Int)]
countChars str =
  let
    chars = unique str
    counted = map (\char -> (char, count str char)) chars
  in sortOn snd counted

unique :: Eq a => [a] -> [a]
unique [] = []
unique (x:xs) = (if x `elem` xs then [] else [x]) ++ unique xs
