module Day4 () where
import Text.Regex (mkRegex, matchRegex)
import Data.List (sort, sortBy)

data Room = Room {name :: String, sectorId :: Int, checksum :: String}
  deriving (Show)

parseRoom :: String -> Room
parseRoom str = case matchRegex (mkRegex "([a-z-]+)-([0-9]+)\\[([a-z]+)\\]") str of
  Just [name, sectorId, checksum] -> Room name (read sectorId) checksum
  Nothing -> error $ "Invalid room name " ++ str

validRoom :: Room -> Bool
validRoom (Room name _ checksum) = (generateChecksum name) == checksum

generateChecksum :: String -> [Char]
generateChecksum name = take 5 byFrequency
  where
    cleanName = sort $ filter (/= '-') name
    chars = foldr countChars [] cleanName
    byFrequency = map fst $ sortBy sortCountOrAlphabet chars

sortCountOrAlphabet :: (Char, Int) -> (Char, Int) -> Ordering
sortCountOrAlphabet (charA, countA) (charB, countB)
  | countA < countB = GT
  | countB < countA = LT
  | charA < charB = LT
  | charB < charA = GT
  | otherwise = EQ

countChars :: Char -> [(Char, Int)] ->  [(Char, Int)]
countChars nextChar [] = [(nextChar, 1)]
countChars nextChar (x:xs) = if nextChar == currentChar
  then (currentChar, count+1) : xs
  else (nextChar, 1) : x : xs
  where (currentChar, count) = x

unencrypt :: Room -> Room
unencrypt room = room {name = shiftWhile name id}
  where (Room name id _) = room

shiftWhile :: String -> Int -> String
shiftWhile name 0 = name
shiftWhile name x = shiftWhile (map shiftChar name) (x - 1)

shiftChar :: Char -> Char
shiftChar '-' = '-'
shiftChar 'z' = 'a'
shiftChar x = succ x

main :: IO ()
main = do
  rawLines <- readFile "day_4.txt"
  let rooms = map parseRoom $ lines rawLines
  let sets = map (\(Room name _ checksum) -> (name, generateChecksum name, checksum)) rooms
  let validRooms = filter validRoom rooms
  print $ "Valid room sum: " ++ (show $ sum $ map sectorId validRooms)
  let unencrypted = map unencrypt validRooms
  putStrLn "Unencrypted names:"
  putStrLn $ unlines $ map show unencrypted
  putStrLn "Pole objects:"
  putStrLn $ unlines $ map show
    $ filter (\(Room name _ _) -> (take 9 name) == "northpole") unencrypted
